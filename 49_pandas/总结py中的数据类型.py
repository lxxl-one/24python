import numpy as np
import pandas as pd


# arr = np.array([1, 2, 3])
# print(type(arr))
# print()
#
# print(type(arr[0]))  # <class 'numpy.int32'>
# print(arr.dtype)  # int32
#
# arr2 = np.array([1.0, 2.2, 3.3])
# print(type(arr2[0]))
# print(arr2.dtype)
# print(arr2.itemsize)  # 8字节
#
# arr3 = np.array([1, 2, 3], dtype=np.float32)
# print(type(arr3[0]))
# print(arr3.dtype)
# print()

ls = [[1, 2, 'a'], ["a", "b", "c"], [1.1, 10, 3.3]]
arr4 = np.array(ls)
print(arr4)
print(arr4.dtype)
print()

print(arr4[0])
print(arr4[0].dtype)
print(type(arr4[0]))

print("-" * 100)


import pandas as pd

ls = [1,2.2,'a']
s = pd.Series(ls)
print(s)
print(s.dtype)  # object

print(s.values)
print(s.values.dtype)  # object
print("-" * 100)



d2 =[{"name" : "a:xiaohong" ,"age" :32,"tel" :10010},
     { "name": "b:xiaogang" ,"tel": 10000} ,
     {"name":"c:xiaowang" ,"age":22}]
df = pd.DataFrame(d2)
print(df)
print()

print(df['name'].str.split(':'))
print()

print(df['name'].tolist())
print(df['name'].str.split(':').tolist())
print("-"*100)

df1 =df.copy()
df1.drop(columns=['age'],axis=1,inplace=True)
print(df1)
df1.drop(index=[0, 1], axis=0, inplace=True)
print(df1)
print("-"*100)

# 创建两个DataFrame
df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3'],
                    'C': ['C0', 'C1', 'C2', 'C3'],
                    'D': ['D0', 'D1', 'D2', 'D3']},
                   index=[0, 1, 2, 3])
print(df1)

df1['E'] = df1['A'] + df1['B']
print(df1)