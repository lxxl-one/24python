"""
id相当于取地址&
一切皆引用的真正含义：
没有什么含义，a、b、num、num1都只是标签，或者说这个标签就是它指向的东西的别名，而别名就是引用
当标签=xxx时，相当于把标签贴在了别的地方，并不影响原处
至于要改变标签所指东西的内容，只能是可变类型数据，且要通过它们的接口
"""

a = 1
b = a

print(id(a))
print(id(b))

b = 2
print(id(b))
print(id(a))  # 改变b，只是把b挂到别的地方，不会改变a

print("~" * 50)


def change_num(num1):  # 相当于函数内部也有一个临时对象num1，指向和传入的num同一块空间
    """
    参考上面的
    a = 1
    b = a
    传参进来，形参num1 = 实参num
    而在函数内部，看不到实参num，对形参num1打印当然和实参num相同
    当形参num1 += 1时，相当于上面的 b = 2，和实参num已经不是同一个指向了，实参num相当于上面的a，当然也不受影响
    """
    print(num1)
    print(id(num1))
    num1 += 1
    print(num1)
    print(id(num1))
    print()
    return num1  # 返回的也是引用


num = 3
print(id(num))
print()

num2 = change_num(num)
print(num2)
print(id(num2))
num2 += 1
print(num2)
print(id(num2))
print()

print(num)
print(id(num))
