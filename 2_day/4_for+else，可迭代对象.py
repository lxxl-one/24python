for i in range(1, 10, ):  # 左闭右开
    if i == 15:
        print("找到15了")
        break
    else:
        print(i, end=" ")
else:  # 当遍历到结尾，即中途没有break，就会执行else，不需要对i进行判断。这是cpp没有的for+else
    print("没有15")
