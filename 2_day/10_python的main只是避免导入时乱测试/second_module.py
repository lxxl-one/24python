def hello():
    print("hello")
    print(num)


if __name__ == '__main__':
    num = 100
    # num是全局变量，如果写在这里，在该模块中是可以使用hello()，
    # 但是导入的地方不会加载main函数，也就没有定义那么导入的地方就没有定义num，调用hello函数时就会报错
    hello()
