import first_module

print(first_module.__name__)  # first_module
print(__name__)  # __main__

# 所以导入的if __name__ == '__main__':
#                   hello()
# 此时这一行的__name__就是first_module.__name__，即first_module，而不是__main__，所以这个主函数不执行
# 也就是说，只有当自己执行自己时，__name__才会等于__main__
