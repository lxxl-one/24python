"""
什么是可变：list、dict、set，这些提供了接口的，通过调用这些接口，可以在不改变地址的情况下操作里面的数据。
            如果不使用接口，则和不可变一样
什么是不可变：只能通过重新赋值去“改变”里面数据，其实是把标签贴到了别的地方，地址当然变了

# 不可变类型，内存中的数据不允许被修改：
# 数字类型 int, bool, float, complex,
# 字符串 str
# 元组 tuple
#
# 可变类型，内存中的数据可以被修改：
# 列表 list
# 字典 dict
# 集合 set
"""


def change(its):
    # its = its.copy()   # 如果这里是copy，则its就是新开辟的一份了，地址和传进来的实参不同
    print(its)
    print(id(its))
    its.append(5)
    print(its)
    print(id(its))

    its = [555, 444]  # 不使用接口，则和不可变变量一样
    print(its)
    print(id(its))


def test1():
    demo_list = [1, 2, 3, 4]
    print(demo_list)
    print(id(demo_list))
    print()
    change(demo_list)
    print()
    print(demo_list)
    print(id(demo_list))


def test2():
    demo_list = [1, 2, 3]
    print(demo_list)
    print(id(demo_list))
    demo_list1 = demo_list
    print(demo_list1)
    print(id(demo_list1))

    print()
    demo_list.append(999)
    print(demo_list)
    print(id(demo_list))

    print("\n只有通过提供的接口才能不改变地址")
    demo_list = [1, 2, 3, 999, 888]
    print(demo_list)
    print(id(demo_list))

    print("~" * 70)
    demo_dict = {"name": "小明"}
    print(demo_dict)
    print(id(demo_dict))
    demo_dict["name"] = "老王"
    print(demo_dict)
    print(id(demo_dict))


if __name__ == '__main__':
    test2()
