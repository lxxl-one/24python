def demo():
    global num  # 说明函数里使用的是全局num，即10，后面将它改成100
    print(num)
    num = 100
    print(num)


num = 10

demo()
print(num)

print("~" * 70)


def demo2():
    num = 20  # 新创建的局部变量，覆盖了全局变量
    print(num)


demo2()
print(num)  # demo2中的num并不影响全局变量num
