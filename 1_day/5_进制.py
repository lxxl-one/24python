num = 123
print(bin(num))
print(oct(num))
print(hex(num))

f = 12345678912345678.9
print(f)

f = 123.45678912345679123456789
print(f)

m = 1.234e6
print(m)
