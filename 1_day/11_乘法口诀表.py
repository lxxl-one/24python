for i in range(1, 10):
    for j in range(1, i + 1):
        print(f"{j} x {i} = {i * j}\t",
              end="")  # 在Python中，end=""是print()函数的一个参数，用于指定在打印输出结束时追加的字符，默认情况下是换行符\n。当使用end=""时，表示在打印输出结束时不加换行符，即不换行。
    print()
