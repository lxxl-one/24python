print(ord('a'))
print(ord('A'))
# 只能获取一个字符

str1 = 'a'
print(ord(str1) - 32)
print(chr(ord(str1) - 32))  # chr把整型换成对应的字符
print(chr(66))
