str1 = '"a'
str2 = "'a"
str3 = '\\'  # 反斜杠\用于转义，当要输出一个反斜杠\时，必须\\
str4 = '\\\\'
str5 = '\"\'abc'
print(type(str1))
print(type(str2))
print(str1)
print(str2)
print(str3)
print(str4)
print(str5)

print("~" * 50)

str6 = "hello\nworld"
print(str6)

str7 = "abc\rd"
print(str7)  # 这是pycharm的问题
