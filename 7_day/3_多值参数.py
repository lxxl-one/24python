def demo2(*args, **kwargs):
    print(f'*args吃掉所有的位置参数，整体作为一个元组{args}，\n'
          f'**kwargs吃掉所有keyword参数，整体作为一个字典{kwargs}')


def demo1(num, *args, **kwargs):
    """
    位置参数要放在所有keyword参数之前
    :param num:可有可无，有就吃掉第一个参数
    :param args:吃掉所有的位置参数，整体作为一个tuple
    :param kwargs:吃掉所有keyword参数，整体作为一个dict
    :return:
    """
    print(f'num吃掉第一个参数{num}，\n'
          f'*args吃掉所有的位置参数，整体作为一个元组{args}，\n'
          f'**kwargs吃掉所有keyword参数，整体作为一个字典{kwargs}')

    print()
    print("多值参数拆包，再传递")
    demo2(*args, **kwargs)  # 拆包即 demo2(2,3,'小明',age = 18,height=175.5,nickname='ming'

    print()
    demo2(args, kwargs)  # 不拆包，则demo2((2, 3, '小明'),{'age': 18, 'height': 175.5, 'nickname': 'ming'})，全是位置参数了


# demo1(1, 2, 3, 4, name="小明", age=18, gender="man",5) # 位置参数要放在所有keyword参数之前
demo1(1, 2, 3, '小明', age=18, height=175.5, nickname='ming')
