class Dog():
    def __init__(self,age):
        self.age = age

    def bark(self):
        print(f"{self.age}汪汪叫")


class XiaoTian(Dog):

    def bark(self, who):
        """
        只有重写，没有覆盖。
        参数不同也是重写
        :return:
        """
        #Dog.bark(self) # 这两句效果一样。万一继承的不是Dog，这里还要改代码。所以采用下面的写法
        super().bark()  # super是一个类，super()就是父亲的匿名对象
        # 这里的bark，如果访问了属性，则访问的是子类的属性，而不是父亲的属性

        print(f"我是{who}")

    def fly(self):
        print("会飞")


xiaotian = XiaoTian(19)
xiaotian.bark("哮天犬")
