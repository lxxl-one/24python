# [-5,256]整数的地址都被写死了
# 不可变对象，贴向相同值时，共享这个相同值
a = 5
b = 5
print(id(a))
print(id(b))
a = 257
b = 257
print(id(a))
print(id(b))
c = 1.1
d = 1.1
print(id(c))
print(id(d))
print()

# intern机制，维护一个dict，
# 已创建字符串hello------>hello的地址，
# 每次创建字符串对象都会和这个dict比较，没有就创建，有就把str2贴到已有的hello上
str1 = 'hello'
str2 = 'hello'
print(id(str1))
print(id(str2))
str1 = 'wo'
str2 = 'ni'
print(id(str1))
print(id(str2))
str2 = 'hello'
print(id(str2))  # 和之前一样，所以dict中的hello没有被释放

# 可变对象则地址不一样，因为list1改变了其中内容，则list2不知道什么时候会变
# 注意和list2=list1这种赋值区分
print()
list1 = [1, 2, 3]
list2 = [1, 2, 3]
print(id(list1))
print(id(list2))
print(list1 == list2)  # 判断相等
print(list1 is list2)  # 判断地址是否相等
