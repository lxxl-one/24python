a = (1, 2, 3)
b = ('a', 'b', 'c')
print(list(zip(a, b)))  # zip是一个类，所以这里创建了一个可迭代的元组对象。然后使用列表生成式，生成了一个list
print(zip(a, b))  # 直接打印可迭代对象是没有输出的

a = [1, 2, 3]
b = ('a', 'b', 'c')
list1 = list(zip(a, b))  # 只要位置对应即可
print(list1)
for i in list1:
    print(i, end=' ')
    print(type(i))

print()
season = ['spring', 'summer', 'fall', 'winter']
print(enumerate(season))  # enumerate也是一个类，创建可迭代的元组对象
season_list = list(enumerate(season))
print(season_list)
print('构建字典')
season_dict = {}
for i in season_list:
    season_dict[i[1]] = i[0]
print(season_dict)
