class Person:
    def __init__(self, name, age, height):
        self.name = name
        self.age = age
        self.height = height

    def run(self):
        print(f"{self.name} is running")

    def eat(self):
        print("eat")

    def __str__(self):
        """
        一般用来展示对象的属性
        :return:
        """
        # return self.name
        # print(f'{self.name}的身高是{self.height}')    #为了print(xiaoming)不报错，最好用return
        return f'{self.name}的身高是{self.height}'

    def __del__(self):
        """
        屁用没有
        :return:
        """
        print("对象将被销毁")


xiaoming = Person("小明", 18, 180.5)
xiaohong = Person("小红", 17, 170)
xiaoming.run()
print(id(xiaoming))
print(id(xiaohong))
print(xiaoming is xiaohong)

print()
XiaoMing = xiaoming  # 说明还是贴标签。
print(xiaoming is XiaoMing)
# python中的类创建对象都是通过__new__，这个对象在内存上，然后才是初始化方法__init__

print()
#xiaohong.__del__()  # 并没有真正释放，除了打印那句话，没啥用
print(xiaoming)  # 如果没有重写str方法，则这两句都是返回对象的地址
print(xiaohong.__str__())
del xiaohong    # 真要释放，就用del。而且这里会执行__del__中的打印
#print(xiaohong) # 报错，del已经把xiaohong释放掉了
print("················程序即将结束··················")
