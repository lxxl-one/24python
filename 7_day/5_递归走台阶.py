def step(num):
    if 1 == num or 2 == num:
        return num
    return step(num - 1) + step(num - 2)


print(step(5))
