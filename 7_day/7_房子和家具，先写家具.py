# 谁被拥有，就先写谁。被偏爱的的都有恃无恐
class HouseItem:
    def __init__(self, name, area):
        self.name = name
        self.area = area

    def __str__(self):
        return f'{self.name}的占地面积是{self.area}'


class House:
    def __init__(self, area1):
        self.area1 = area1
        self.area2 = area1
        self.item_list = []

    def __str__(self):
        return f'房子已有家具{self.item_list}, 剩余面积{self.area2}'

    def add_item(self, item: HouseItem):  # 注解，为了pycharm联想
        if self.area2 >= item.area:
            self.item_list.append(item.name)
            self.area2 -= item.area
        else:
            print(f"空间不够，放不下{item.name}")


bed = HouseItem("席梦思", 4)
chest = HouseItem("衣柜", 2)
table = HouseItem("餐桌", 1.5)

house = House(60)
house.add_item(bed)
print(house.__str__())
house.add_item(chest)
print(house.__str__())
house.add_item(table)
print(house.__str__())
