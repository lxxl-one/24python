class A:
    def demo1(self):
        print("A demo1")

    def demo2(self):
        print("A demo2")

    def demo3(self):
        print("A demo3")


class B:
    def demo1(self):
        print("B demo1")

    def demo2(self):
        print("B demo2")

    def demo3(self):
        print("B demo3")


class C(A, B):
    def demo1(self):
        print("C demo1")

    def demo2(self):
        #super().demo2()  # 默认C后面的第一个，即A
        #super(B, self).demo2()  # 报错，B之后没有类了
        super(A, self).demo2()   # 调用继承顺序中 A之后 的 那个类的方法，顺序CAB，A之后是B，所以调用的是B的方法


c = C()
c.demo1()
c.demo2()
c.demo3()     # C中没有处理demo3，默认使用mro
print(C.__mro__)  # (<class '__main__.C'>, <class '__main__.A'>, <class '__main__.B'>, <class 'object'>)
