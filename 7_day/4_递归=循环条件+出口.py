def print_num(num):
    print(num)
    if 1 == num:
        return  # return作为出口
    print_num(num - 1)  # 循环
    print("---", num, "---")  # 函数执行完毕，也是出口


print_num(3)
