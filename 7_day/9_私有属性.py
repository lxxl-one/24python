class A:
    def __init__(self):
        self.__num1 = 1

    def display(self):
        print(self.__num1)


class B(A):
    def __init__(self):
        super().__init__()
        self.num2 = 2

    def display(self):
        #print(self.__num1)   #报错，AttributeError: 'B' object has no attribute '_B__num1'。不能访问A的私有属性
        super().display()
        print(self.num2)
        print("我是重写的display")


a = A()
a.display()
b = B()
b.display()
