import numpy as np
from matplotlib import pyplot as plt

us_file_path = "./youtube_video_data/US_video_data_numbers.csv"
uk_file_path = "./youtube_video_data/GB_video_data_numbers.csv"

# t1 = np.loadtxt(us_file_path,delimiter=",",dtype="int",unpack=True)   # unpack实现转置的效果
t_uk = np.loadtxt(uk_file_path, delimiter=",", dtype="int")

# 取出来英国的youtube评论数和喜欢数,第1列是like数，第3列是comment数
# print(t_uk[:,1].max(),t_uk[:,1].min())
t_uk = t_uk[t_uk[:, 1] <= 500000]  # 筛选like数小于等于500000的视频
t_uk_like = t_uk[:, 1]
# print(t_uk_like.max(), t_uk_like.min())
t_uk_comment = t_uk[:, -1]

# 散点图，看一看是不是like越多，comment越多
plt.figure(figsize=(20, 8), dpi=80)
plt.scatter(t_uk_like, t_uk_comment)
plt.show()
