import numpy as np
import matplotlib.pyplot as plt

us_file_path = "./youtube_video_data/US_video_data_numbers.csv"
uk_file_path = "./youtube_video_data/GB_video_data_numbers.csv"

t1 = np.loadtxt(us_file_path, delimiter=",", dtype="int", unpack=True)
t2 = np.loadtxt(us_file_path, delimiter=",", dtype="int")

print(t1)
print("-" * 100)
print(t2)
print("-" * 100)

b = t2[2:5, 1:4]
print(b)

t_us = t2
# 取评论的数据
t_us_comments = t_us[:, -1]
print(t_us.shape)
print("-" * 100)

# 打印最大和最小值，过滤掉一些太大的数值
print(t_us_comments.max(), t_us_comments.min())
t_us_comments = t_us_comments[t_us_comments <= 5000]
print(t_us_comments.max(), t_us_comments.min())
print(t_us_comments.shape)  # 过滤完剩多少数据
d = 50
bin_nums = (t_us_comments.max() - t_us_comments.min()) // d

# 绘图
plt.figure(figsize=(20, 8), dpi=80)  # figure是绘图的窗口，figsize是长宽，dpi是分辨率
plt.hist(t_us_comments, bin_nums)
plt.show()
