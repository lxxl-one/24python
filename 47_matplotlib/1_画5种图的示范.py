import matplotlib
import matplotlib.pyplot as plt
from matplotlib import font_manager
import numpy as np
import random


plt.figure(figsize=(10, 8), dpi=80)

"""================================================================================"""
# arr_x = np.arange(0, 24, 2)
# arr_y = np.array([random.randint(15, 30) for _ in range(12)])

# x_ticks_label = [f'{i}:00' for i in arr_x]
# y_ticks_label = [f'{i}°C' for i in range(arr_y.min(), arr_y.max()+1)]

# plt.xticks(arr_x, x_ticks_label,rotation=45)
# plt.yticks(range(arr_y.min(), arr_y.max()+1), y_ticks_label)
# plt.plot(arr_x, arr_y, label='line')
# plt.show()

"""================================================================================"""
# plt.xticks(arr_x, x_ticks_label,rotation=45)
# plt.yticks(range(arr_y.min(), arr_y.max()+1), y_ticks_label)
# plt.scatter(arr_x, arr_y, label='scatter')
# plt.show()

"""================================================================================"""
# a = ['流浪地球', '疯狂的外星人', '飞驰人生', '大黄蜂', '熊出没·原始时代', '新喜剧之王']
# b = [38.13, 19.85, 14.89, 11.36, 6.47, 5.93]
# my_font = font_manager.FontProperties(fname='C:\Windows\Fonts\SimSun.ttc', size=16)

# # 绘制条形图
# rects = plt.bar(range(len(a)), b, width=0.3, color='r')  # 绘制条形图，第一个参数是条的数量，第二个参数是条的高度
# plt.xticks(range(len(a)), a, fontproperties=my_font, rotation=45)  # 设置x轴标签，不然x轴显示012345，不是电影名

# # 在条形图上加标注(水平居中)
# for rect in rects:  # 遍历每一个条形图，rects是条形图的列表
#     height = rect.get_height()  # 获取条形图的高度
#     plt.text(rect.get_x() + rect.get_width() / 2, height + 0.5, str(height), ha="center")
#     # ha水平居中，str(height)是要显示的文字，heigh+0.5是要显示的文字放在Y轴的高度，往上偏移0.5
#     # get_x()获取条形图的x坐标,get_width()获取条形图的宽度，所以第一个参数就是条形图的中心点，就是要显示的文字所放的x轴位置

# plt.show()
"""================================================================================"""

# time = [131, 98, 125, 131, 124, 139, 131, 117, 128, 108, 135, 138, 131, 102, 107, 114,
#         119, 128, 121, 142, 127, 130, 124, 101, 110, 116, 117, 110, 128, 128, 115, 99,
#         136, 126, 134, 95, 138, 117, 111, 78, 132, 124, 113, 150, 110, 117, 86, 95, 144,
#         105, 126, 130, 126, 130, 126, 116, 123, 106, 112, 138, 123, 86, 101, 99, 136, 123,
#         117, 119, 105, 137, 123, 128, 125, 104, 109, 134, 125, 127, 105, 120, 107, 129, 116,
#         108, 132, 103, 136, 118, 102, 120, 114, 105, 115, 132, 145, 119, 121, 112, 139, 125,
#         138, 109, 132, 134, 156, 106, 117, 127, 144, 139, 139, 119, 140, 83, 110, 102, 123,
#         107, 143, 115, 136, 118, 139, 123, 112, 118, 125, 109, 119, 133, 112, 114, 122, 109,
#         106, 123, 116, 131, 127, 115, 118, 112, 135, 115, 146, 137, 116, 103, 144, 83, 123,
#         111, 110, 111, 100, 154, 136, 100, 118, 119, 133, 134, 106, 129, 126, 110, 111, 109,
#         141, 120, 117, 106, 149, 122, 122, 110, 118, 127, 121, 114, 125, 126, 114, 140, 103,
#         130, 141, 117, 106, 114, 121, 114, 133, 137, 92, 121, 112, 146, 97, 137, 105, 98,
#         117, 112, 81, 97, 139, 113, 134, 106, 144, 110, 137, 137, 111, 104, 117, 100, 111,
#         101, 110, 105, 129, 137, 112, 120, 113, 133, 112, 83, 94, 146, 133, 101, 131, 116,
#         111, 84, 137, 115, 122, 106, 144, 109, 123, 116, 111, 111, 133, 150]
#
#
# my_font = font_manager.FontProperties(fname='C:\Windows\Fonts\SimSun.ttc', size=18)
#
#
# print(max(time), min(time))
# # 设置组距
# distance = 2
# # 计算组数
# group_num = int((max(time) - min(time)) / distance)  # 最大值减去最小值除以组距就得到组数
# plt.hist(time, bins=group_num)
#
# # 修改x轴刻度显示
# plt.xticks(range(min(time), max(time) + 2)[::2])
#
# plt.grid(linestyle="--", alpha=0.5)
#
#
# plt.xlabel("电影时长大小", fontproperties=my_font)
# plt.ylabel("电影的数据量", fontproperties=my_font)
#
# plt.show()

"""================================================================================"""

size = [55, 35, 10]  # 各部分大小
explode = [0, 0.1, 0]  # 各部分突出值
color = ["red", "green", "blue"]  # 各部分颜色
label_list = ["第一部分", "第二部分", "第三部分"]  # 各部分标签

matplotlib.rcParams['font.sans-serif'] = ['SimHei']  #设置字体
patches, l_text, p_text = plt.pie(size, explode=explode, colors=color, labels=label_list,
                                  labeldistance=1.1, autopct="%1.1f%%", shadow=True, startangle=90,
                                  pctdistance=0.6)
plt.axis("equal")  # 设置横轴和纵轴大小相等，这样饼才是圆的
plt.legend()
plt.show()