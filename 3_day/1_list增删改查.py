demo_list = ["zhangsan", "lisi", "wangwu", "lisi"]
# 查
print(demo_list[0])
print(demo_list.index("lisi"))  # 只返回第一个
# print(demo_list.index("lis")) 报错

# 修改
print()
demo_list[0] = "李四"
print(demo_list)

# 增加
print()
demo_list.append("老六")
print(demo_list)
demo_list.insert(1, "老四")
# demo_list[7]="1"   #没有字典这种改法
print(demo_list)

demo_list1 = [1, 2, "3"]
demo_list.extend(demo_list1)
print(demo_list)

# 删除
print()
demo_list.remove("lisi")  # 只删除第一个
print(demo_list)
demo_list.pop()
print(demo_list)
demo_list.pop(2)
print(demo_list)
demo_list.clear()
print()
