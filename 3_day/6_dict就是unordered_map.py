demo_dict = {"name": "小明"}
print(demo_dict['name'])
# print(demo_dict[0]) 报错，因为字典是无序容器

demo_dict['age'] = 18
print(demo_dict)
demo_dict.pop('name')
print(demo_dict)
print()

demo_dict = {"name": "小明", "age": 18}
demo_dict1 = {"nick_name": "小红", "age": 19}
demo_dict.update(demo_dict1)  # 原有key相同，则新值会覆盖旧值
print(demo_dict)
print()

print("遍历：")
for k in demo_dict:
    print(f'{k}---{demo_dict[k]}')
for kv in demo_dict.items():
    print(f'{kv}')
print()

info_list = [{'name': '小明', 'age': 18}, {'name': '李红', 'age': 29}]
print(info_list)
print()

# 可迭代对象只会解除最外面一层
for item in info_list:
    print(item)
    print('-'*20)
