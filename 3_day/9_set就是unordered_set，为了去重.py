empty_dict = {}
empty_set = set()

x = {"apple", "banana", "cherry", "apple"}
y = {"google", "apple", "microsoft"}
print(x)
print(y)
print(x.difference(y))  # x有y没有的
print(y.intersection(x))  # xy都有的

print("\n将字符串set化")
str1 = "abcdacedfa"
str2 = "mnoempoace"
set1 = set(str1)
set2 = set(str2)
print(set1)
print(set2)
print()
print(set1 - set2)  # 差集，和difference一样
print(set1 | set2)  # 并集
print(set1 & set2)  # 交集
print(set1 ^ set2)  # 并-交，即各怀鬼胎


