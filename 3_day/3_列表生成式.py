a = [x for x in range(10)]
print(a)

b = [j for i in range(10) for j in range(i)]  # 先i后j
print(b)

c = [x for x in range(10) if x % 2 == 0]
print(c)

# 二维列表、二维转一维
print()
d = [[col * row for col in range(5)] for row in range(5)]  # 先外层后内层
print(d)
e = [j for i in d for j in i]  # 先前面后后面
print(e)

print()
f = [x if x % 2 == 0 else x ** 2 for x in range(10)]
print(f)
