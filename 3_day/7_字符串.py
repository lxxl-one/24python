str1 = 'hello world'
for i in str1:
    print(i, end='')
print()
print(len(str1))
print(str1.count('l'))
print(str1[1])
print(str1.index('lo'))  # 有则是第一个字母的index
# print(str1.index('et'))没有则报错

print()
str2 = "hellohellohello"
print(str2.replace("he", "HE", 2))

print()
str3 = " 123 456 sr  "
print(str3.strip())  # 去除左右所有两边空格
print(str3.strip("sr"))  # 只去除字符串

print()
str4 = 'hello'
str_list = list(str4)
print(str_list)
# 拼接列表中的元素
print(''.join(str_list))
print(' '.join(str_list))

print()
str5 = "how are you"
print(str5.split())  # 默认以空格划分字符串
str6 = "how,are,you"
print(str6.split(','))  # 指定逗号划分
print(str6.split(',', 1))  # 只分隔出num+1个子串

print()
str7 = "hello\nworld\nnihao\n "
print(str7.splitlines())
