demo_tuple = ("zhangsan", 15, "1.75", 15, "15")
print(demo_tuple[0])
print(demo_tuple.index("15"))
print(demo_tuple.count(15))
print(len(demo_tuple))

print()
demo_tuple1 = ()
print(type(demo_tuple1))
demo_tuple2 = (1)
print(type(demo_tuple2))
demo_tuple3 = (1,)
print(type(demo_tuple3))

print()
info_tuple = ("小明", 25, 1.75)
print("我的名字是 %s，年龄 %d，身高%.2f" % info_tuple)
# 本质是一个字符串
str1 = "我的名字是 %s，年龄 %d，身高%.2f" % info_tuple
print(str1)

print()
"""
list 、tuple其实都是类，
相互转换也就是创建了新的实例
"""
print(demo_tuple)
print(id(demo_tuple))
demo_list = list(demo_tuple)
print(demo_list)
print(id(demo_list))
demo_tuple = tuple(demo_list)
print(demo_tuple)
print(id(demo_tuple))
