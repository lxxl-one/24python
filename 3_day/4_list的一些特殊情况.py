a = [1, 3, 5]
b = [2, 4]
print(a)
print(id(a))

print()
a = a * 2  # 数目翻倍，比如要写50个0，c=[0]*50
print(a)
print(id(a))

print()
a = a + b
print(a)
print(id(a))

print()
a += b  # 相当于extend
print(a)
print(id(a))
