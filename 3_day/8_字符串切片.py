num_str = "0123456789"

# 开头是0，末位是-1
# []完依然是字符串
print(num_str[2:6])  # 2~5
print(num_str[2:])  # 2~末位
print(num_str[:5])  # 开头5个
print(num_str[0:])  # 完整
print(num_str[::2])  # 每隔一个
print(num_str[2:-1])  # 2~末位-1
print(num_str[-2:])  # 最后2个
print(num_str[::-1])  # 逆序  ''.join(list(num_str).reverse())

list1 = list(num_str)
list1.reverse()
print(''.join(list1))
