import random  # 如果当前目录或上一级目录等最近目录下也有random.py，则导入它而非系统下的
# 但有的pycharm也会智能选择优先导入系统库的

print(random.__file__)  # D:\3.8\lib\random.py
# 比如上一级目录有random.py 则 E:\24PythonCode\24python\8_day\random.py
# 比如当前目录有random.py，则 E:\24PythonCode\24python\8_day\9_模块使用\random.py
print(random.randint(0, 10))
