from mymodule import say_hello  # 使用高频的可以指定导入
from mymodule import Dog
from mymodule2 import say_hello as say_hello2 #两个模块有冲突的，则要起别名

say_hello()
say_hello2()
dog = Dog("Tom")
dog.bark()
