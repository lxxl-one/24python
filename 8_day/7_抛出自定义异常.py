def input_passwd():
    pwd=input('输入密码：')
    if len(pwd) >=8:
        return pwd
    print("raise抛出异常")
    # ex=Exception('pwd is too short')
    # raise ex
    raise Exception('pwd is too short')
    #print(1)  #raise后面的代码不会执行


try:
    print(input_passwd())
except Exception as e:
    print(e)
