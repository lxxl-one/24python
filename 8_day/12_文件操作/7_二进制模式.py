# 打开图片、音乐、电影，要用二进制模式
# 下面实现 复制图片
pic_file = open("test.png", mode="rb")
# print(file)
# print(file.read())
pic = pic_file.read()

pic_file_copy = open("test_copy.png", "wb")
pic_file_copy.write(pic)

pic_file.close()
pic_file_copy.close()
