file = open("file.txt", encoding='utf-8')  # 默认打开方式是r只读
text1 = file.read()
print(text1)

file = open("file2.txt", encoding="utf-8", mode="w+")
# text = file.read()  # w只写，不能读
# 注意，w打开，如果存在该文件，会先把文件清空。没有则创建
# 但是，如果是先读了，光标走到最后，这时在w打开，则不会清空前面的
