file = open("file3.txt", mode="r+")
text = file.read()
print(text)
print(type(text))  # str
print(len(text))  # 17

print()
file = open("file3.txt", mode="rb+")
text = file.read()
print(text)
print(type(text))  # 读出来的类型是字节流bytes，而不是字符串str
print(len(text))  # 19  每行结束是 \r\n

# windows下，内存的换行符是\n，文件中的是\r\n，采用文本模式打开文件，会自动把\r\n转为\n，而二进制模式还是\r\n
# 如果所读文件file3.txt编码是LF（比如linux下），则文件中的换行符都是\n，文本模式和二进制模式打开都一样
