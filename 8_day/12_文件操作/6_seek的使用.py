import os

file = open("file2.txt", mode="r+")
file.seek(5, os.SEEK_SET)  # 偏移量，相对于哪里偏移
#file.seek(5,1)  # 使用除了os.SEEK_SET或0外的位置参数，报错。
# 解决办法：把r+改成rb+，
# 同时二进制打开下不能 encoding = "xxx"   ValueError: binary mode doesn't take an encoding argument
text = file.read(6)
print(text)
