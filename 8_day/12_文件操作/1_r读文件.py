file = open("file.txt", encoding='utf-8')  # encoding默认和系统一致。windows是gbk，而linux和mac下的默认编码都是utf-8
text1 = file.read()  # 默认参数为读取全部字符。最后的空白行也能读到
print(text1)
print(file)

print("\n此时文件流已经走到最后了")
text2 = file.read()
print(text2)
file.close()
