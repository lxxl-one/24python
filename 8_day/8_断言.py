try:
    # 如果单独写assert，不用try捕获
    assert 1 == 0, '如果前面判断为假，则抛出这里这句话作为异常内容，异常类型为AssertionError'
    # AssertionError: 如果前面判断为假，则抛出这里这句话作为异常内容，异常类型为AssertionError
except Exception as e:
    print(e)    # 捕获到了 如果前面判断为假，则抛出这里这句话作为异常内容，异常类型为AssertionError
