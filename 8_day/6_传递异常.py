"""
当最底层的demo1发生异常时，这个异常会自动层层抛出，在任意高层可以将其捕获
main主线程<-----demo2<-----demo1

"""


def demo1():
    return int(input("输入整数："))


def demo2():
    num = demo1()
    print(f"拿到整数{num}")
    return num


#demo2()

# 在主线程中被捕获
try:
    demo2()
except Exception as e:
    print(e)
