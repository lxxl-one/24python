class MusicPlayer:
    instance = None

    def __new__(cls, *args, **kwargs):
        print("重写了new函数")

        instance = super().__new__(cls)
        print(id(instance))
        return instance

    def __init__(self):
        print("播放器初始化")


kugou = MusicPlayer()
print(id(kugou))
