class Tool:
    count = 0  # 类属性

    def __init__(self, name):
        self.name = name
        Tool.count += 1

    @classmethod
    def show_tool_count(cls):  # cls是类名自身
        print(cls.count)  # 类方法就是为了访问类属性
        # print(self.name) 报错，类方法不能访问对象属性

    @staticmethod
    def show_help():  # 区别就是：静态方法没有cls指针，不访问类属性，一般就是打印一些全局的提示信息
        print("可以创建工具了")


Tool.show_help()
tool1 = Tool("斧头")
tool2 = Tool("马来剑")
tool3 = Tool("铁铲")
print(f'创建了 {Tool.count} 个对象')
Tool.show_tool_count()

"""
如果不加classmethod
"""
# Tool.show_tool_count(Tool) #如果去掉classmethod装饰器，则这里要传入一个类名

# tool3.show_tool_count(Tool)
# #删掉classmethod，TypeError: show_tool_count() takes 1 positional argument but 2 were given
# 说明此时它就是一个对象方法，则隐含self，所以在定义时要  def show_tool_count(self,cls):
