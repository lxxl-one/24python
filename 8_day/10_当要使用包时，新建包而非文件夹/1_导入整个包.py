import wd

# wd.send_message.send()  # 1
# wd.send()  # 2
wd.send1()  # 3
wd.send_message.send()  # 强写，虽然init中没有直接导入send_message，但是其他语句让它可见了
# wd.send_message.send1()    # 报错，强写，但是send_message中没有send1
print()

txt = wd.receive_message.receive()
print(txt)
print()

print(wd.__file__)
# E:\24PythonCode\24python\8_day\10_当要使用包时，新建包而非文件夹\wd\__init__.py
print(wd.receive_message.__file__)
# E:\24PythonCode\24python\8_day\10_当要使用包时，新建包而非文件夹\wd\receive_message.py
#print(wd.receive_message.receive.__file__) # 没有这玩意
