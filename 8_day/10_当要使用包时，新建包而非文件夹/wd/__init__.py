# init指明了，外面能看见包里的哪些模块、函数
# 不写在init的模块、函数，外面 import 包 后不可见
# 当外面直接使用 包.某函数()时，正是因为init里直接 from . 某模块 import 某函数

from . import receive_message

#from . import send_message  # 1
#from .send_message import send    # 对外直接暴露模块send_message的函数send，而非模块send_message  # 2
from .send_message import send as send1  # 对外暴露函数别名send1，而非send  # 3
