try:
    num = int(input("输入一个整数："))
    result = 8 / num
    print(result)
# except ValueError:
#     print("要求输入整数，而不是英文字符")
except ZeroDivisionError:
    print("不能输入0")
except Exception as e:
    # Exception是一个类，e就是该类创建的一个对象，e=异常信息，
    # 比如 ValueError: invalid literal for int() with base 10: 'a'    冒号后面的内容
    print(f"未知错误{e}")
    print(e.__traceback__.tb_frame.f_globals["__file__"])  # 异常所在文件。比如a模块调用b模块的函数，那边出的问题，这里能追溯到
    print(e.__traceback__.tb_lineno)  # 异常所在行数
else:
    print("程序执行正常，即try成功，没有经过except")
finally:
    print("无论什么情况，都会进入这里")
