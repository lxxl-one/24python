class MusicPlayer:
    instance = None

    def __new__(cls, *args, **kwargs):
        """
        为什么要写多值参数？
        因为创建对象时先调用new，要把参数传递给init
        :param args:
        :param kwargs:
        """

        if cls.instance is None:
            cls.instance = super().__new__(cls)

        return cls.instance

    def __init__(self, name):
        self.name = name
        print(f"{self.name} 播放器初始化")


kugou = MusicPlayer("酷狗")
print(id(kugou))
kugou1 = MusicPlayer("酷狗1")
print(id(kugou1))
kugou2 = MusicPlayer("酷狗2")
print(id(kugou2))
print("~" * 50)
print(id(kugou))
print(kugou.name)  # 此时kugou的name已经被改为酷狗2了
