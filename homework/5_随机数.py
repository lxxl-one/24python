import random

# print(random.randint(1, 3))#返回[a,b]之间的整数

player = int(input("请出拳，石头1，剪刀2，布3："))

computer = random.randint(1, 3)

if player == computer:
    print("平局")
elif (player == 1 and computer == 2) or (player == 2 and computer == 3) or (player == 3 and computer == 1):
    print("你赢了")
else:
    print("你输了")
