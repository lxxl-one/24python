s = int(input("输入整数"))

if s >= 0:
    # 正数补码=正数原码
    num = bin(s).count('1')
else:
    # 对于负数
    s1 = s & 0xFFFFFFFFFFFFFFFF
    # num = 64 - bin(-s - 1).count('1')  # 64位，-s-1为原负数补码取反
    num = bin(s1).count('1')  # 注意，bin得到二进制数，其实是字符串。count是统计字符串中'1‘的个数的方法

print("%d 的2进制中1的个数为%d" % (s, num))
