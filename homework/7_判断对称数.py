while True:
    try:
        num = int(input("输入一个整数："))
        break
    except ValueError:
        print("必须输入整数")

str_num = str(num)
i = 0
j = len(str_num) - 1
while i <= j:
    try:
        if (str_num[i] != str_num[j]):
            print(f"{num}不是对称数")
            raise Exception("输入的数要求是对称数")
    except Exception as e:
        print(e)
        break

    i += 1
    j -= 1

if i > j:
    print(f"{num}是对称数")
