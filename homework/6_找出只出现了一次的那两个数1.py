"""
核心就是把这两个数分开，
偶数出现的数随便分去哪边都无所谓
"""
def findSingleNumbers(arr):
    xorResult = 0
    num1, num2 = 0, 0

    for i in arr:
        xorResult ^= i

    # xorResult - 1将最低位的1置为0，比它低的全从0变为1
    # 取反，即整个数字，除了最低位的1还是1，其他都相反
    # 按位与，setBit = 整个数字最低位的1保留，其他位全是0
    setBit = xorResult & ~(xorResult - 1)

    for i in arr:
        if i & setBit:  # 这个位置为1的数字进行异或
            print(f'第一堆{i}')
            num1 ^= i
        else:  # 这个位置为0的数字进行异或
            print(f'第二堆{i}')
            num2 ^= i

    print("只出现一次的两个数字为：{} 和 {}".format(num1, num2))


arr = [1, 2, 3, 4, 5, 1, 2, 3, 4, 7]
findSingleNumbers(arr)
