def find_two(mylist: list):
    if len(mylist) == 2:
        return mylist

    result = [0, 0]
    res = mylist[0]
    for i in range(1, len(mylist)):
        res ^= mylist[i]

    pos = 0
    for i in range(32):
        if (res >> i) & 1 == 1:
            pos = i
            break

    for i in mylist:
        if i >> pos & 1 == 1:
            result[0] ^= i
        else:
            result[1] ^= i

    return result


yourlist = [1, -4, 2, 2]
print(find_two(yourlist))
