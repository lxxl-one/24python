def get_twos_complement(num):
    if num >= 0:
        return bin(num & 0xFFFFFFFFFFFFFFFF)[2:].zfill(64)  # 正数的补码为其原码
    else:
        return bin((abs(num) ^ 0xFFFFFFFFFFFFFFFF) + 1)[2:].zfill(64)  # 负数的补码


# 测试
num = -5
twos_complement = get_twos_complement(num)
print(f"Number: {num}")
print(f"Twos complement: {twos_complement}")
